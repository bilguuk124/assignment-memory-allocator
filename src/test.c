


#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"
#include <sys/mman.h>
#ifndef MAP_FIXED

#define MAP_FIXED 0x100000

#endif

void debug(const char *fmt, ...);

static struct block_header* get_block_header (void *data){
    return (struct block_header*)(((uint8_t*)data)-offsetof(struct block_header, contents));
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd );

static void* block_after(struct block_header const* block);
static inline void* heap_last(struct block_header* first);
static void* map_pages(void const *addr, size_t length, int additional_flags);

void* heap;

bool test1 (){
    debug("Starting test1: initializing heap \n");
    heap = heap_init(1000);
    if(heap == NULL){
        err("Test 1 failed. Heap allocator not working \n");
        return false;
    }
    debug_heap(stdout, heap);
    debug("Test 1 passed. \n");
    return true;
}
bool test2() {
    debug("Starting test2: Allocating and freeing a block \n");
    void* block1 = _malloc(100);
    if(!block1){
        err("Block allocator not working \n");
        return false;
    }
    debug_heap(stdout,heap);
    _free(block1);
    struct block_header* header = get_block_header(block1);
    if(!(header->is_free)){
        err("Test 2 failed. Block freer not working \n");
        return false;
    }
    debug_heap(stdout, heap);
    debug("Test 2 passed \n");
    return true;
}

bool test3 (){
    debug("Starting test3: Allocating and freeing multiple blocks\n");
    void* block1 = _malloc(100);
    void* block2 = _malloc(100);
    debug_heap(stdout,heap);
    if(!block1 || !block2){
        err("Test 3 failed. Block initializer not working \n");
        return false;
    } 
    _free(block1);
    struct block_header* header1 = get_block_header(block1);
    struct block_header* header2 = get_block_header(block2);
    if(!header1->is_free && header2->is_free){
        err("Test 3 failed. Block freer not working \n");
        return false;
    } 
    _free(block2);
    if(!header1->is_free || !header2->is_free){
        err("Test 3 failed. Block freer not working \n");
        return false;
    }
    debug_heap(stdout, heap);
    debug("Test 3 passed \n");
    return true;
}


bool test4(){
    debug("Starting test4: adjacency check \n");
    void* block1 = _malloc (100);
    void* block2 = _malloc(100);
    void* block3 = _malloc(100);
    if(!block1 || !block2 || !block2){
        err("Null blocks \n");
        return false;
    }
    debug_heap(stdout, heap);
    struct block_header* header1 = get_block_header(block1);
    struct block_header* header2 = get_block_header(block2);
    struct block_header* header3 = get_block_header(block3);
    if(!blocks_continuous(header1, header2) || !blocks_continuous(header2, header3)){
        err("Blocks are not adjacent. \n");
        return false;
    }
    debug_heap(stdout, heap);
    _free(block1);
    _free(block2);
    _free(block3);
    debug("Test 4 passed \n");
    return true;
}

bool test5(){
    debug("Starting test5 : heap growth check \n");
    void* block1 = _malloc(10000);
    if(!block1){
        err("Test 5 failed. Heap growth failure. \n");
        return false;
    }
    debug_heap(stdout, heap);
    debug("Test 5 passed \n");
    return true;
}
bool test6(){
	debug("Starting test6: new region");
	debug_heap(stdout, heap);
	void* block1 = _malloc (10000);
	struct block_header* header1 = get_block_header(block1);
	void* last = heap_last(header1);
	map_pages(last, 1000, MAP_FIXED);
	debug_heap (stdout, heap);
	void* block2 = _malloc(10000);
	if(blocks_continuous(last, get_block_header(block2))) {
		err("Error: block are not adjacent \n");
		return false;
	}
    debug_heap(stdout,heap);
	debug("Test 6 passed");
	_free(block1);
	_free(block2);
	return true;
}

bool run_tests(){
    if(test1()){
        bool check = test2() && test3() && test4() && test5() && test6();
        check == true ? debug("All tests have passed! Respect! \n") : debug("Not all tests have passed try again. \n");
        return check;
    } else return false;
}

static void* map_pages(void const *addr, size_t length, int additional_flags){
	return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

static inline void* heap_last (struct block_header *first){
	while(first->next != NULL) first = first->next;
	return (void*)block_after(first);
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}


static void* block_after(struct block_header const* block){
    return (void*)(block->contents + block->capacity.bytes);
}


